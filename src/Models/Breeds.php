<?php

namespace App\Models;

class Breeds
{
    public $data;

    /**
     * Construtor para inicializar o objeto e buscar uma imagem de cachorro aleatória.
     *
     * @param string $breed A raça do cachorro a ser buscada (padrão: 'shiba').
     */
    public function __construct($breed = 'shiba')
    {
        $url = "https://dog.ceo/api/breed/$breed/images/random";

        $response = file_get_contents($url);

        $this->data = $response;
    }
}