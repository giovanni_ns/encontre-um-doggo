<?php

namespace App\Models;

class BreedsList
{
    public $data;

    /**
     * Construtor da classe BreedsList.
     *
     * Este construtor realiza uma requisição para a API Dog CEO para obter uma lista de raças de cachorros.
     * Em seguida, organiza os dados e armazena no atributo $data da classe.
     */
    public function __construct()
    {
        $url = 'https://dog.ceo/api/breeds/list/all';

        $response = file_get_contents($url);

        $data = json_decode($response, true);

        $arrayData = array(array_keys($data['message']));

        $this->data = json_encode(["breeds" => $arrayData]);
    }
}