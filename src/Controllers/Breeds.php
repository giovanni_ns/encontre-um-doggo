<?php

namespace App\Controllers;

class Breeds
{
    /**
     * Método para exibir informações sobre uma raça de cão em formato JSON.
     */
    public function index()
    {
        header("Content-type: application/json");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $raca = $_REQUEST['dog-breed'];

            $breeds = new \App\Models\Breeds($raca);
            echo $breeds->data;

        } else {
            echo 'Página não encontrada';
        }
    }
}