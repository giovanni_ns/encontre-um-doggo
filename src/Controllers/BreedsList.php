<?php

namespace App\Controllers;

class BreedsList
{
    /**
     * Método para exibir a lista de raças de cães em formato JSON.
     */
    public function index()
    {
        header("Content-type: application/json");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $breedslist = new \App\Models\BreedsList;
            echo $breedslist->data;

        } else {
            echo 'Página não encontrada';
        }
    }
}