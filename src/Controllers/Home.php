<?php

namespace App\Controllers;

class Home
{
    public function index()
    {
        include_once(__DIR__ . '\..\Views\head.html');
        include_once(__DIR__ . '\..\Views\Home\index.html');
    }
}