<?php

require(__DIR__ . '\vendor\autoload.php');

$routes = [
    '/' => 'App\Controllers\Home@index',
    '/api/breeds' => 'App\Controllers\Breeds@index',
    '/api/breeds/list' => 'App\Controllers\BreedsList@index',
];

$uri = $_SERVER['REQUEST_URI'];

if (array_key_exists($uri, $routes)) {
    list($controller, $action) = explode('@', $routes[$uri]);

    $controller = new $controller();
    $controller->$action();
    
} else {
    echo 'Página não encontrada';
}
